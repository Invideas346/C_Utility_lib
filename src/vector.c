//
// Created by Wolfgang Aigner on 26.10.2021.
//

#include <vector.h>
#include <string.h>
#include <typedef.h>
#include <log.h>

#define VECTOR_INDEX(index) ((index) * (vector->object_size))

inline static void assign_error_code(VECTOR_ERROR_CODE* ec, VECTOR_ERROR_CODE value)
{
    if(ec != NULL)
        *ec = value;
}

static void* push_back(Vector* vector, const void* data, VECTOR_ERROR_CODE* ec)
{
    if(!vector->is_initialized) {
        assign_error_code(ec, VECTOR_NOT_INITIALIZED);
        LOG_ERROR("Vector not intialized");
        return NULL;
    }
    if(vector->object_size == 0)
        return NULL;

    vector->count += 1;
    void* newAddress = realloc(vector->data, vector->count * vector->object_size);

    if(newAddress == NULL) {
        assign_error_code(ec, VECTOR_MEMORY_ALLOCATION_ERROR);
        LOG_ERROR("Could not allocate memory for vector");
        return NULL;
    }

    vector->data = newAddress;
    memcpy(vector->data + VECTOR_INDEX(vector->count - 1), data, vector->object_size);
    assign_error_code(ec, VECTOR_OK);

    return vector->data + VECTOR_INDEX(vector->count - 1);
}

static void* push_front(Vector* vector, const void* data, VECTOR_ERROR_CODE* ec)
{
    if(!vector->is_initialized) {
        assign_error_code(ec, VECTOR_NOT_INITIALIZED);
        LOG_ERROR("Vector not intialized");
        return NULL;
    }
    if(vector->object_size == 0)
        return NULL;

    vector->count += 1;
    void* newAddress = realloc(vector->data, vector->count * vector->object_size);
    if(newAddress == NULL) {
        assign_error_code(ec, VECTOR_MEMORY_ALLOCATION_ERROR);
        LOG_ERROR("Could not allocate memory for vector");
        return NULL;
    }

    vector->data = newAddress;
    if(vector->count != 1) {
        memcpy(vector->data + VECTOR_INDEX(1), vector->data + VECTOR_INDEX(0),
               vector->object_size * (vector->count - 1));
    }
    memcpy(vector->data + VECTOR_INDEX(0), data, vector->object_size);
    assign_error_code(ec, VECTOR_OK);
    return vector->data + VECTOR_INDEX(0);
}

static uint8_t pop_back(Vector* vector, VECTOR_ERROR_CODE* ec)
{
    if(!vector->is_initialized) {
        assign_error_code(ec, VECTOR_NOT_INITIALIZED);
        LOG_ERROR("Vector not intialized");
        return FALSE;
    }
    if(vector->count == 0 || vector->object_size <= 0) {
        LOG_WARNING("Index out-of-bounds");
        return FALSE;
    }
    vector->count -= 1;
    void* newAddress = realloc(vector->data, vector->count * vector->object_size);
    if(newAddress == NULL) {
        assign_error_code(ec, VECTOR_MEMORY_ALLOCATION_ERROR);
        LOG_ERROR("Could not allocate memory for vector");
        return FALSE;
    }
    vector->data = newAddress;
    assign_error_code(ec, VECTOR_OK);
    return TRUE;
}

static uint8_t pop_front(Vector* vector, VECTOR_ERROR_CODE* ec)
{
    if(!vector->is_initialized) {
        assign_error_code(ec, VECTOR_NOT_INITIALIZED);
        LOG_ERROR("Vector not intialized");
        return FALSE;
    }
    if(vector->count == 0 || vector->object_size <= 0) {
        LOG_WARNING("Index out-of-bounds");
        return FALSE;
    }
    vector->count -= 1;
    void* temp = malloc(vector->object_size * vector->count);
    if(temp == NULL) {
        assign_error_code(ec, VECTOR_MEMORY_ALLOCATION_ERROR);
        LOG_ERROR("Could not allocate memory for vector");
        return FALSE;
    }
    memcpy(temp, vector->data + VECTOR_INDEX(1), vector->count * vector->object_size);
    void* newAddress = realloc(vector->data, vector->count * vector->object_size);
    if(newAddress == NULL) {
        assign_error_code(ec, VECTOR_MEMORY_ALLOCATION_ERROR);
        LOG_ERROR("Could not allocate memory for vector");
        free(temp);
        return FALSE;
    }
    vector->data = newAddress;
    memcpy(vector->data, temp, vector->count * vector->object_size);
    free(temp);
    assign_error_code(ec, VECTOR_OK);
    return TRUE;
}

static void* at(Vector* vector, uint32_t position, VECTOR_ERROR_CODE* ec)
{
    if(!vector->is_initialized) {
        assign_error_code(ec, VECTOR_NOT_INITIALIZED);
        LOG_ERROR("Vector not intialized");
        return NULL;
    }
    if(vector->count == 0 || vector->object_size <= 0) {
        LOG_WARNING("Index out-of-bounds");
        return NULL;
    }
    assign_error_code(ec, VECTOR_OK);
    return vector->data + VECTOR_INDEX(position);
}

static uint8_t resize(Vector* vector, uint32_t num_elements, VECTOR_ERROR_CODE* ec)
{
    if(!vector->is_initialized) {
        assign_error_code(ec, VECTOR_NOT_INITIALIZED);
        LOG_ERROR("Vector not intialized");
        return FALSE;
    }
    if(vector->object_size <= 0 || num_elements < 0) {
        LOG_WARNING("Object size 0 or number elements 0 or smaller");
        return FALSE;
    }
    void* newAddress = realloc(vector->data, vector->object_size * num_elements);
    if(newAddress == NULL) {
        assign_error_code(ec, VECTOR_MEMORY_ALLOCATION_ERROR);
        LOG_ERROR("Could not allocate memory for vector");
        return FALSE;
    }
    vector->data = newAddress;
    vector->count = num_elements;
    assign_error_code(ec, VECTOR_OK);
    return TRUE;
}

static Vector* copy_heap(Vector* vector, VECTOR_ERROR_CODE* ec)
{
    if(!vector->is_initialized) {
        assign_error_code(ec, VECTOR_NOT_INITIALIZED);
        LOG_ERROR("Vector not intialized");
        return NULL;
    }
    Vector* copy = vector_init_heap_data(vector->count, vector->data, vector->object_size, ec);
    assign_error_code(ec, VECTOR_OK);
    return copy;
}

static Vector copy_stack(Vector* vector, VECTOR_ERROR_CODE* ec)
{
    if(!vector->is_initialized) {
        assign_error_code(ec, VECTOR_NOT_INITIALIZED);
        LOG_ERROR("Vector not intialized");
        return *vector;
    }
    Vector copy = vector_init_stack_data(vector->count, vector->data, vector->object_size, ec);
    assign_error_code(ec, VECTOR_OK);
    return copy;
}

static void clear(Vector* vector, VECTOR_ERROR_CODE* ec)
{
    if(!vector->is_initialized) {
        assign_error_code(ec, VECTOR_NOT_INITIALIZED);
        LOG_ERROR("Vector not intialized");
        return;
    }
    free(vector->data);
    vector->data = NULL;
    vector->count = 0;
    assign_error_code(ec, VECTOR_OK);
}

static void for_each(Vector* vector,
                     void (*func)(void* data, uint32_t index, uint32_t object_size),
                     VECTOR_ERROR_CODE* ec)
{
    if(!vector->is_initialized) {
        assign_error_code(ec, VECTOR_NOT_INITIALIZED);
        LOG_ERROR("Vector not intialized");
        return;
    }
    for(uint32_t i = 0; i < vector->count; ++i) {
        func(vector->at(vector, i, ec), i, vector->object_size);
        if(ec != NULL)
            if(*ec != VECTOR_OK)
                break;
    }
}

inline static void assign_methods(Vector* vector)
{
    vector->push_back = push_back;
    vector->push_front = push_front;
    vector->pop_back = pop_back;
    vector->pop_front = pop_front;
    vector->at = at;
    vector->resize = resize;
    vector->copy_heap = copy_heap;
    vector->copy_stack = copy_stack;
    vector->clear = clear;
    vector->for_each = for_each;
}

Vector* vector_init_heap(uint32_t size, uint32_t object_size, VECTOR_ERROR_CODE* ec)
{
    Vector* vector = (Vector*) malloc(sizeof(Vector));
    if(vector == NULL) {
        assign_error_code(ec, VECTOR_MEMORY_ALLOCATION_ERROR);
        LOG_ERROR("Could not allocate memory for vector");
        return NULL;
    }
    vector->count = size;
    vector->object_size = object_size;
    vector->data = malloc(size * object_size);
    if(vector->data == NULL) {
        assign_error_code(ec, VECTOR_MEMORY_ALLOCATION_ERROR);
        LOG_ERROR("Could not allocate memory for vector");
        return vector;
    }
    assign_methods(vector);
    vector->is_initialized = TRUE;
    assign_error_code(ec, VECTOR_OK);
    return vector;
}

Vector* vector_init_heap_data(uint32_t size,
                              const void* data,
                              uint32_t object_size,
                              VECTOR_ERROR_CODE* ec)
{
    Vector* vector = (Vector*) malloc(sizeof(Vector));
    if(vector == NULL) {
        assign_error_code(ec, VECTOR_MEMORY_ALLOCATION_ERROR);
        LOG_ERROR("Could not allocate memory for vector");
        return NULL;
    }
    vector->count = size;
    vector->object_size = object_size;
    vector->data = malloc(size * object_size);
    if(vector->data == NULL) {
        assign_error_code(ec, VECTOR_MEMORY_ALLOCATION_ERROR);
        LOG_ERROR("Could not allocate memory for vector");
        return vector;
    }
    memcpy(vector->data, data, size * object_size);
    assign_methods(vector);
    vector->is_initialized = TRUE;
    assign_error_code(ec, VECTOR_OK);
    return vector;
}

Vector vector_init_stack(uint32_t size, uint32_t object_size, VECTOR_ERROR_CODE* ec)
{
    Vector vector;
    vector.count = size;
    vector.object_size = object_size;
    vector.data = malloc(size * object_size);
    if(vector.data == NULL) {
        assign_error_code(ec, VECTOR_MEMORY_ALLOCATION_ERROR);
        LOG_ERROR("Could not allocate memory for vector");
        return vector;
    }
    assign_methods(&vector);
    vector.is_initialized = TRUE;
    assign_error_code(ec, VECTOR_OK);
    return vector;
}

Vector vector_init_stack_data(uint32_t size,
                              const void* data,
                              uint32_t object_size,
                              VECTOR_ERROR_CODE* ec)
{
    Vector vector;
    vector.count = size;
    vector.object_size = object_size;
    vector.data = malloc(size * object_size);
    if(vector.data == NULL) {
        assign_error_code(ec, VECTOR_MEMORY_ALLOCATION_ERROR);
        LOG_ERROR("Could not allocate memory for vector");
        return vector;
    }
    memcpy(vector.data, data, size * object_size);
    assign_methods(&vector);
    vector.is_initialized = TRUE;
    assign_error_code(ec, VECTOR_OK);
    return vector;
}

void vector_deinit(Vector* vector, VECTOR_ERROR_CODE* ec)
{
    if(!vector->is_initialized) {
        assign_error_code(ec, VECTOR_NOT_INITIALIZED);
        LOG_ERROR("Vector not intialized");
        return;
    }
    free(vector->data);
    vector->data = NULL;
    vector->count = 0;
    vector->object_size = 0;
    vector->is_initialized = FALSE;
    assign_error_code(ec, VECTOR_OK);
}