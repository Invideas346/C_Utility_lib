//
// Created by Wolfgang Aigner on 26.10.2021.
//

#ifndef __Vector_H__
#define __Vector_H__

#include <stdlib.h>
#include <stdint.h>

/**
 * @brief This enum represents every possible error when working with vectors.
 */
typedef enum VECTOR_ERROR_CODE {
    VECTOR_OK = 0,
    VECTOR_GENERAL_ERROR = 1,
    VECTOR_NOT_INITIALIZED = 2 | VECTOR_GENERAL_ERROR,
    VECTOR_MEMORY_ALLOCATION_ERROR = 3 | VECTOR_GENERAL_ERROR
} VECTOR_ERROR_CODE;

typedef struct Vector Vector;

/**
 * @brief This struct represents a vector. A vector is essentially a dynamic vector.
 */
typedef struct Vector {
    void* data;
    uint32_t object_size;
    uint32_t count;

    uint8_t is_initialized;

    /**
     * @brief Removes all indices and frees the occupied heap memory.
     * @param vector
     * @param ec
     */
    void (*clear)(Vector* vector, VECTOR_ERROR_CODE* ec);

    /**
     * @brief Adds an item to the back of the vector.
     * @param vector
     * @param data
     * @param ec
     * @return Returns the pointer to the added item.
     * Returns NULL if a error occurred.
     */
    void* (*push_back)(Vector* vector, const void* data, VECTOR_ERROR_CODE* ec);

    /**
     * @brief Adds an item to the front of the vector.
     * @param vector
     * @param data
     * @param ec
     * @return Returns the pointer to the added item.
     * Returns NULL if a error occurred.
     */
    void* (*push_front)(Vector* vector, const void* data, VECTOR_ERROR_CODE* ec);

    /**
     * @brief Removes the last item of the vector.
     * @param vector
     * @param ec
     * @return Returns whether the operation was successful.
     */
    uint8_t (*pop_back)(Vector* vector, VECTOR_ERROR_CODE* ec);

    /**
     * @brief Removes the first item of the vector.
     * @param vector
     * @param ec
     * @return Returns whether the operation was successful.
     */
    uint8_t (*pop_front)(Vector* vector, VECTOR_ERROR_CODE* ec);

    /**
     * @brief Returns the pointer to the specified element within the vector.
     * @note The pointer returned points directly to the element in the vector.
     * @param vector
     * @param position
     * @param ec
     * @return Returns the pointer to the specified element within the vector.
     * Returns NULL if a error occurred.
     */
    void* (*at)(Vector* vector, uint32_t position, VECTOR_ERROR_CODE* ec);

    /**
     * @brief Resizes the vector with the specified number of elements.
     * @param vector
     * @param numElements
     * @param ec
     * @return Returns whether the operation was successful.
     */
    uint8_t (*resize)(Vector* vector, uint32_t numElements, VECTOR_ERROR_CODE* ec);

    /**
     * @brief Returns a copy of itself.
     * @param vector
     * @param ec
     */
    Vector* (*copy_heap)(Vector* vector, VECTOR_ERROR_CODE* ec);

    /**
     * @brief Returns a copy of itself.
     * @param vector
     * @param rror_code
     */
    Vector (*copy_stack)(Vector* vector, VECTOR_ERROR_CODE* ec);

    /**
     * @brief Simple implementation of a higher order function.
     * @param vector
     * @param func
     * @param code
     * @param ec
     */
    void (*for_each)(Vector* vector,
                     void (*func)(void* data, uint32_t index, uint32_t objectSize),
                     VECTOR_ERROR_CODE* ec);
} Vector;

/**
 * @brief Creates a new dynamic vector initialized with all function pointers.
 * @return Vector*
 */
Vector* vector_init_heap(uint32_t size, uint32_t object_size, VECTOR_ERROR_CODE* ec);
/**
 * @brief Creates a new dynamic vector initialized with all function pointers.
 * @return Vector*
 */
Vector* vector_init_heap_data(uint32_t size,
                              const void* data,
                              uint32_t object_size,
                              VECTOR_ERROR_CODE* ec);

/**
 * @brief Creates a new dynamic vector initialized with all function pointers.
 * @return Vector*
 */
Vector vector_init_stack(uint32_t size, uint32_t object_size, VECTOR_ERROR_CODE* ec);
/**
 * @brief Creates a new dynamic vector initialized with all function pointers.
 * @return Vector*
 */
Vector vector_init_stack_data(uint32_t size,
                              const void* data,
                              uint32_t object_size,
                              VECTOR_ERROR_CODE* ec);

/**
 * @brief Deinitializes the vector and frees the occupied heap memory.
 */
void vector_deinit(Vector* vector, VECTOR_ERROR_CODE* ec);

#endif  // __Vector_H__