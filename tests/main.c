#include <stdlib.h>
#include <stdio.h>
#include <cutil.h>

#include <check.h>

/* ###### Vector tests ###### */
START_TEST(Vector_clear_empty)
{
    VECTOR_ERROR_CODE ec;
    Vector vector = vector_init_stack(0, sizeof(int), &ec);

    vector.clear(&vector, &ec);

    ck_assert_int_eq(vector.data, NULL);
    ck_assert_int_eq(vector.count, 0);
    ck_assert_int_eq(ec, VECTOR_OK);
    vector_deinit(&vector, &ec);
}
END_TEST

START_TEST(Vector_clear)
{
    int val = 1;
    VECTOR_ERROR_CODE ec;
    Vector vector = vector_init_stack(0, sizeof(int), &ec);

    for(int i = 0; i < 1000; i++)
        vector.push_back(&vector, &val, &ec);

    vector.clear(&vector, &ec);

    ck_assert_int_eq(vector.data, NULL);
    ck_assert_int_eq(vector.count, 0);
    ck_assert_int_eq(ec, VECTOR_OK);
    vector_deinit(&vector, &ec);
}
END_TEST

START_TEST(Vector_push_back_empty)
{
    int val = 1;
    VECTOR_ERROR_CODE ec;
    Vector vector = vector_init_stack(0, sizeof(int), &ec);

    vector.push_back(&vector, &val, &ec);

    ck_assert_int_eq(*(int*) vector.data, val);
    ck_assert_int_eq(vector.count, 1);
    ck_assert_int_eq(ec, VECTOR_OK);
    vector_deinit(&vector, &ec);
}
END_TEST

START_TEST(Vector_push_back)
{
    int val = 1;
    VECTOR_ERROR_CODE ec;
    Vector vector = vector_init_stack(0, sizeof(int), &ec);

    vector.push_back(&vector, &val, &ec);
    val = 2;
    vector.push_back(&vector, &val, &ec);

    ck_assert_int_eq(*(int*) vector.at(&vector, 1, NULL), 2);
    ck_assert_int_eq(vector.count, 2);
    ck_assert_int_eq(ec, VECTOR_OK);
    vector_deinit(&vector, &ec);
}
END_TEST

START_TEST(Vector_push_front_empty)
{
    int val = 1;
    VECTOR_ERROR_CODE ec;
    Vector vector = vector_init_stack(0, sizeof(int), &ec);

    vector.push_front(&vector, &val, &ec);

    ck_assert_int_eq(*(int*) vector.data, val);
    ck_assert_int_eq(vector.count, 1);
    ck_assert_int_eq(ec, VECTOR_OK);
    vector_deinit(&vector, &ec);
}
END_TEST

START_TEST(Vector_push_front)
{
    int val = 1;
    VECTOR_ERROR_CODE ec;
    Vector vector = vector_init_stack(0, sizeof(int), &ec);

    vector.push_front(&vector, &val, &ec);
    val = 2;
    vector.push_front(&vector, &val, &ec);

    ck_assert_int_eq(*(int*) vector.at(&vector, 0, NULL), 2);
    ck_assert_int_eq(vector.count, 2);
    ck_assert_int_eq(ec, VECTOR_OK);
    vector_deinit(&vector, &ec);
}
END_TEST

START_TEST(Vector_pop_back)
{
    int val = 1;
    VECTOR_ERROR_CODE ec;
    Vector vector = vector_init_stack(0, sizeof(int), &ec);

    vector.push_back(&vector, &val, &ec);
    val = 2;
    vector.push_back(&vector, &val, &ec);
    vector.pop_back(&vector, NULL);

    ck_assert_int_eq(*(int*) vector.at(&vector, vector.count - 1, NULL), 1);
    ck_assert_int_eq(vector.count, 1);
    ck_assert_int_eq(ec, VECTOR_OK);
    vector_deinit(&vector, &ec);
}
END_TEST

START_TEST(Vector_pop_front)
{
    int val = 1;
    VECTOR_ERROR_CODE ec;
    Vector vector = vector_init_stack(0, sizeof(int), &ec);

    vector.push_back(&vector, &val, &ec);
    val = 2;
    vector.push_back(&vector, &val, &ec);
    vector.pop_front(&vector, NULL);

    ck_assert_int_eq(*(int*) vector.at(&vector, vector.count - 1, NULL), 2);
    ck_assert_int_eq(vector.count, 1);
    ck_assert_int_eq(ec, VECTOR_OK);
    vector_deinit(&vector, &ec);
}
END_TEST

Suite* vector_test_suite(void)
{
    Suite* s = suite_create("Vector");
    TCase* tc_clear = tcase_create("Clear");
    tcase_add_test(tc_clear, Vector_clear);
    tcase_add_test(tc_clear, Vector_clear_empty);

    TCase* tc_push_back = tcase_create("Push back");
    tcase_add_test(tc_push_back, Vector_push_back);
    tcase_add_test(tc_push_back, Vector_push_back_empty);
    suite_add_tcase(s, tc_push_back);

    TCase* tc_push_front = tcase_create("Push front");
    tcase_add_test(tc_push_front, Vector_push_front);
    tcase_add_test(tc_push_front, Vector_push_front_empty);
    suite_add_tcase(s, tc_push_front);

    TCase* tc_pop_back = tcase_create("Pop back");
    tcase_add_test(tc_pop_back, Vector_pop_back);
    suite_add_tcase(s, tc_pop_back);

    TCase* tc_pop_front = tcase_create("Pop front");
    tcase_add_test(tc_pop_front, Vector_pop_front);
    suite_add_tcase(s, tc_pop_front);
    return s;
}

int main(void)
{
    Suite* s = vector_test_suite();
    SRunner* sr = srunner_create(s);

    srunner_run_all(sr, CK_NORMAL);  // Run all tests
    int number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? 0 : 1;
}